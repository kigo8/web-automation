import faker from "faker";

const generateUniqueEmail = () => {
  return Date.now() + faker.internet.email();
};

const generateUniquePassword = () => {
  return faker.internet.password();
};

const generateUniqueLastname = () => {
  return faker.name.lastName();
};

export { generateUniqueEmail, generateUniquePassword, generateUniqueLastname };
