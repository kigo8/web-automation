Feature: Booking

  This feature tests register funconality and booking funconality

  @reg
  Scenario: Register
    Given user is on home page
    When user presses Register button
    And user enters valid email
    And user presses Get Started button
    And user enters valid passwords
    And user presses Create Account button
    Then user is signed in

  Scenario: Booking
    Given user is signed in
    When user selects "Sky High Hotel Airport" as destination
    And user selects dates from "2020-06-21" to "2020-06-22"
    And user selects 2 adults, 1 children and 1 room
    And user presses Search button
    And user reserves first hotel available
    And user clicks Reserve Your Selections button
    And user clicks I'll Reserve button
    And user enters the details
    And user clicks Final Details button
    And user selects payment method "Pay online by"
    And user selects Complete Booking button
    Then booking details are correct