class SearchResultsPage {
  get apartmentApplyButton() {
    return $(".sr_property_block .sr_cta_button");
  }
  get apartmentTitle() {
    return $(".sr-hotel__name");
  }
}

export default new SearchResultsPage();
