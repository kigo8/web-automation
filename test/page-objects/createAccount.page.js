class CreateAccountPage {
  get emailInput() {
    return $("input[type='email']");
  }

  get getStartedButton() {
    return $("button[type='submit']");
  }
}

export default new CreateAccountPage();
