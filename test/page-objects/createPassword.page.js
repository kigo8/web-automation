class CreatePasswordPage {
  get createPasswordInput() {
    return $("#password");
  }
  get confirmPasswordInput() {
    return $("#confirmed_password");
  }
  get createAccountButton() {
    return $("button[type='submit']");
  }
}

export default new CreatePasswordPage();
