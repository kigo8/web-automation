class HomePage {
  get serchBoxTitle() {
    return $(".sb-searchbox__title-text");
  }
  get registerButton() {
    return $("#current_account_create");
  }
  get profileMenu() {
    return $("#profile-menu-trigger--title");
  }
  get destinationInput() {
    return $("input[type='search']");
  }
  get calendarButton() {
    return $(".xp__dates");
  }
  get calendarBox() {
    return $(".bui-calendar[style*='display:block']");
  }
  get guestsButton() {
    return $(".xp__input-group.xp__guests");
  }
  get guestsBox() {
    return $("#xp__guests__inputs-container");
  }
  get increaseChildrenButton() {
    return $("button[aria-label='Increase number of Children']");
  }
  get searchButton() {
    return $(".sb-searchbox__button");
  }
  getCalendarDateCell(dateCell) {
    return $(`td[data-date="${dateCell}"]`);
  }
}

export default new HomePage();
