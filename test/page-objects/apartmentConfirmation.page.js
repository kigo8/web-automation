class ApartmentConfirmationPage {
  get reserveYourSelectionButton() {
    return $(".submitButton");
  }
  get illReserveButton() {
    return $(".js-reservation-button");
  }
}

export default new ApartmentConfirmationPage();
