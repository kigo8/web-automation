class PersonalDetailsPage {
  get lastNameInput() {
    return $("input[name='lastname']");
  }
  get finalDetailsButton() {
    return $(".bui-button--primary");
  }
}

export default new PersonalDetailsPage();
