import { When, Then } from "cucumber";
import SearchResultsPage from "../page-objects/searchResults.page";
import ApartmentConfirmationPage from "../page-objects/apartmentConfirmation.page";
import PersonalDetailsPage from "../page-objects/personalDetails.page";
import { generateUniqueLastname } from "../utils/randomGenerator";

When("user reserves first hotel available", () => {
  SearchResultsPage.apartmentApplyButton.waitForClickable();
  SearchResultsPage.apartmentApplyButton.click();

  const apartmentTitle = SearchResultsPage.apartmentTitle.getText();

  browser.switchWindow(apartmentTitle);
});

When("user clicks Reserve Your Selections button", () => {
  ApartmentConfirmationPage.reserveYourSelectionButton.waitForClickable();
  ApartmentConfirmationPage.reserveYourSelectionButton.click();
});

When("user clicks I'll Reserve button", () => {
  browser.pause(1000);
  ApartmentConfirmationPage.illReserveButton.click();
});

When("user enters the details", () => {
  const lastname = generateUniqueLastname();

  PersonalDetailsPage.lastNameInput.setValue(lastname);
});

When("user clicks Final Details button", () => {
  PersonalDetailsPage.finalDetailsButton.waitForClickable();
  PersonalDetailsPage.finalDetailsButton.click();
});

When("user selects payment method {string}", (paymentMethod) => {
  console.log({ paymentMethod });
});

When("user selects Complete Booking button", () => {
  console.log("great success!");
});

Then("booking details are correct", () => {
  console.log("great success!");
});
