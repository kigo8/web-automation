import { Given, When } from "cucumber";
import HomePage from "../page-objects/home.page";
import CreateAccountPage from "../page-objects/createAccount.page";
import CreatePasswordPage from "../page-objects/createPassword.page";
import {
  generateUniqueEmail,
  generateUniquePassword,
} from "../utils/randomGenerator";

Given("user is on home page", () => {
  expect(HomePage.serchBoxTitle).toHaveTextContaining("Find deals on hotels");
});

When("user presses Register button", () => {
  HomePage.registerButton.click();
});

When("user enters valid email", () => {
  const email = generateUniqueEmail();

  CreateAccountPage.emailInput.setValue(email);
});

When("user presses Get Started button", () => {
  CreateAccountPage.getStartedButton.click();
});

When("user enters valid passwords", () => {
  const password = generateUniquePassword();

  CreatePasswordPage.createPasswordInput.setValue(password);
  browser.pause(300); // Sometimes browser insert only part of password in to confirmPasswordInput
  CreatePasswordPage.confirmPasswordInput.setValue(password);
});

When("user presses Create Account button", () => {
  CreatePasswordPage.createAccountButton.waitForClickable();
  CreatePasswordPage.createAccountButton.click();
});
