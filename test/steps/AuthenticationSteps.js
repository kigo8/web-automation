import { Then } from "cucumber";
import HomePage from "../page-objects/home.page";

Then("user is signed in", () => {
  expect(HomePage.profileMenu).toBeDisplayed();
});
