import { When } from "cucumber";
import HomePage from "../page-objects/home.page";

When("user selects {string} as destination", (destination) => {
  HomePage.destinationInput.setValue(destination);
});

When("user selects dates from {string} to {string}", (dateFrom, dateTo) => {
  HomePage.calendarButton.click();
  HomePage.calendarBox.isExisting();

  const dateFromCell = HomePage.getCalendarDateCell(dateFrom);
  const dateToCell = HomePage.getCalendarDateCell(dateTo);

  dateFromCell.waitForClickable();
  dateFromCell.click();
  dateToCell.click();
});

When("user selects 2 adults, 1 children and 1 room", () => {
  HomePage.guestsButton.click();
  HomePage.guestsBox.isExisting();
  HomePage.increaseChildrenButton.click();
});

When("user presses Search button", () => {
  HomePage.searchButton.click();
});
